package com.beehyv.kafka.consumer;

import com.beehyv.kafka.producer.KafkaMessageFeeder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by kapil on 9/10/17.
 *
 * First arg: No of consumer groups to create (consumers will be divided equally)
 * Second arg: No of consumers
 * Third arg: No of topics to subscribe to
 */
public class ConsumerMain {


  public static void main(String[] args) {
    int numConsumers = 3, numTopicsToSubscribe = 0, numConsumerGroups = 0;
    if (args.length > 0) {
      numConsumerGroups = Integer.parseInt(args[0]);
      numConsumers = Integer.parseInt(args[1]);
      numTopicsToSubscribe = Integer.parseInt(args[2]);
    }
    if (numConsumerGroups > numConsumers) {
      System.out.println("No of consumers should be greater than no of consumer groups");
      System.exit(1);
    }
    String consumerGroup = "consumer-twitter-group";
    List<String> topics;
    if (numTopicsToSubscribe > 0) {
      topics = new ArrayList<>(numTopicsToSubscribe);
      for (int i = 0; i < numTopicsToSubscribe; i++) {
        topics.add(i, "my-topic-" + i);
      }
    } else {
      topics = Arrays.asList(KafkaMessageFeeder.TOPICS);
    }
    ExecutorService executor = Executors.newFixedThreadPool(numConsumers);

    final List<ConsumerLoop> consumers = new ArrayList<>();
    int consumerGroupId = 0;
    for (int i = 0; i < numConsumers; i++) {
      if (numConsumerGroups > 0 && i%numConsumerGroups == 0) {
        consumerGroupId++;
      }
      ConsumerLoop consumer = new ConsumerLoop(i, consumerGroup +  "-" + consumerGroupId, topics);
      consumers.add(consumer);
      executor.submit(consumer);
    }

    Runtime.getRuntime().addShutdownHook(new Thread() {
      @Override
      public void run() {
        for (ConsumerLoop consumer : consumers) {
          consumer.shutdown();
        }
        executor.shutdown();
        try {
          executor.awaitTermination(5000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    });
  }

}
