package com.beehyv.kafka.producer;

import com.beehyv.twitter.FeedReceiver;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import twitter4j.Status;
import twitter4j.User;

/**
 * Created by kishore on 21/9/17.
 *
 * http://kafka.apache.org/0110/javadoc/index.html?org/apache/kafka/clients/producer/KafkaProducer.html
 * http://kafka.apache.org/documentation/#api
 */
public class KafkaMessageFeeder implements FeedReceiver {

  private Producer<String, String> producer;
  private Random random = new Random();
  private int numTopics;

  public static final String[] TOPICS = {"my-topic-1337", "my-topic-1338", "my-topic-1339",
      "my-topic-1330", "my-topic-1331", "my-topic-1332", "my-topic-1333", "my-topic-1334",
      "my-topic-1335", "my-topic-1336"};

  public KafkaMessageFeeder(int numTopics) {
    this();
    this.numTopics = numTopics;
  }

  public KafkaMessageFeeder() {
    Properties props = new Properties();
    // props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "ip-172-31-29-32.us-west-2.compute.internal:9092,"
        + "ip-172-31-20-139.us-west-2.compute.internal:9092,"
        + "ip-172-31-30-254.us-west-2.compute.internal:9092");
    props.put(ProducerConfig.ACKS_CONFIG, "all");
    props.put(ProducerConfig.RETRIES_CONFIG, 0);
    props.put(ProducerConfig.BATCH_SIZE_CONFIG, 16384);
    props.put(ProducerConfig.LINGER_MS_CONFIG, 1);
    props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 33554432);
    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

    producer = new KafkaProducer<>(props);
  }

  private void publish(String topic, String key, String value)
      throws ExecutionException, InterruptedException {
    ProducerRecord<String, String> record = new ProducerRecord<>(topic, key, value);
    producer.send(record, new Callback() {
      @Override
      public void onCompletion(RecordMetadata metadata, Exception exception) {
        if (metadata != null)
          System.out.println(metadata);
      }
    });
  }

  @Override
  public void onTweetReceived(Status status) {
    User user = status.getUser();
    String username = user.getScreenName();
    System.out.println(username);
    String profileLocation = user.getLocation();
    System.out.println(profileLocation);
    long tweetId = status.getId();
    System.out.println(tweetId);
    String content = status.getText();
    System.out.println(content +"\n");
    try {
      String topic = TOPICS[random.nextInt(TOPICS.length)];
      if (numTopics > 0)
         topic = "my-topic-" + random.nextInt(numTopics);
      publish(topic, username, content);
    } catch (ExecutionException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void onFailure(Exception e) {
    System.out.println("Exception occurred in Streaming API");
    /*if (producer != null)
      producer.close();*/
  }
}
