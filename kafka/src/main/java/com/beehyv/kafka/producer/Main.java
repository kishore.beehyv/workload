package com.beehyv.kafka.producer;

import com.beehyv.twitter.TwitterFeed;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by kishore on 21/9/17.
 *
 * First arg: No of producers
 * Second arg: No of topics to write to
 */
public class Main {

  private final static String CONSUMER_KEY = "t9hY8ZiM3OqdwfEI2jpJPoBjb";
  private final static String CONSUMER_KEY_SECRET = "TetN4lCjy5SRD86odDi5uD9pWQ3NyrbkQMudYPiAOBtcapIXGw";
  private final static String ACCESS_TOKEN = "855019606204743680-rjJWnFKr35FHPAgOXfvzMDyVQPaCLdl";
  private final static String ACCESS_TOKEN_SECRET = "HDrgo1fvr4W0qcObS62DKARdRGasHfHN9jFZcDXc7TTOu";

  public static void main(String[] args) {
    int numProducers = 1, numTopicsToWrite = 10;
    if (args.length > 0) {
      numProducers = Integer.parseInt(args[0]);
      numTopicsToWrite = Integer.parseInt(args[1]);
    }
    ExecutorService executor = Executors.newFixedThreadPool(numProducers);
    KafkaMessageFeeder kafkaMessageFeeder;
    if (numProducers > 1) {
      kafkaMessageFeeder = new KafkaMessageFeeder(numTopicsToWrite);
    } else {
      kafkaMessageFeeder = new KafkaMessageFeeder();
    }
    TwitterFeed poller = new TwitterFeed(kafkaMessageFeeder, CONSUMER_KEY, CONSUMER_KEY_SECRET,
        ACCESS_TOKEN, ACCESS_TOKEN_SECRET, null);
    for (int i=0; i<numProducers; i++) {
      if (i == 0) {
        executor.submit(poller);
      } else {
        KafkaRandomStringFeeder randomStringFeeder = new KafkaRandomStringFeeder(i, numTopicsToWrite);
        executor.submit(randomStringFeeder);
      }
    }
  }

}
