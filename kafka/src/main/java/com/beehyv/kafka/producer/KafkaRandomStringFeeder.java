package com.beehyv.kafka.producer;

import java.util.Properties;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

/**
 * Created by kapil on 11/10/17.
 */
public class KafkaRandomStringFeeder  implements Runnable {

  private Producer<String, String> producer;
  private Random random = new Random();
  private int id;
  private int numTopics;

  public KafkaRandomStringFeeder(int id, int numTopics) {
    this.id = id;
    this.numTopics = numTopics;
    Properties props = new Properties();
    // props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "ip-172-31-29-32.us-west-2.compute.internal:9092,"
        + "ip-172-31-20-139.us-west-2.compute.internal:9092,"
        + "ip-172-31-30-254.us-west-2.compute.internal:9092");
    props.put(ProducerConfig.ACKS_CONFIG, "all");
    props.put(ProducerConfig.RETRIES_CONFIG, 0);
    props.put(ProducerConfig.BATCH_SIZE_CONFIG, 16384);
    props.put(ProducerConfig.LINGER_MS_CONFIG, 1);
    props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 33554432);
    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

    producer = new KafkaProducer<>(props);
  }

  private void publish(String topic, String key, String value)
      throws ExecutionException, InterruptedException {
    ProducerRecord<String, String> record = new ProducerRecord<>(topic, key, value);
    producer.send(record, new Callback() {
      @Override
      public void onCompletion(RecordMetadata metadata, Exception exception) {
        if (metadata != null)
          System.out.println("Producer " + id + ":" + metadata);
      }
    });
  }

  @Override
  public void run() {
    while (true) {
      String key = "Random Key: " + UUID.randomUUID();
      String value = "Random Value: " + UUID.randomUUID();
      String topic = "my-topic-" + random.nextInt(numTopics);
      try {
        publish(topic, key, value);
      } catch (ExecutionException e) {
        e.printStackTrace();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      if (Thread.interrupted())
        return;
    }
  }
}
