package com.beehyv.twitter;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created by kishore on 28/9/17.
 *
 * Simple class that streams tweets from Twitter Streaming API
 * http://davidcrowley.me/?p=435
 */
public class TwitterFeed implements Runnable {

  private TwitterStream stream;
  private FeedReceiver feedReceiver;
  private String keywords[] = {"candy", "india", "machine", "learning", "artificial", "intelligence"};

  public TwitterFeed(FeedReceiver feedReceiver, String consumerKey, String consumerSecret,
      String accessToken, String accessTokenSecret, String[] keywords) {

    this.feedReceiver = feedReceiver;

    ConfigurationBuilder cb = new ConfigurationBuilder();
    cb.setDebugEnabled(true);
    cb.setOAuthConsumerKey(consumerKey);
    cb.setOAuthConsumerSecret(consumerSecret);
    cb.setOAuthAccessToken(accessToken);
    cb.setOAuthAccessTokenSecret(accessTokenSecret);

    stream = new TwitterStreamFactory(cb.build()).getInstance();

    if (keywords != null && keywords.length > 0) {
      this.keywords = keywords;
    }
  }

  public void start() {

    StatusListener listener = new StatusListener() {
      public void onStatus(Status status) {
        feedReceiver.onTweetReceived(status);
      }

      public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {

      }

      public void onTrackLimitationNotice(int numberOfLimitedStatuses) {

      }

      public void onScrubGeo(long userId, long upToStatusId) {

      }

      public void onStallWarning(StallWarning warning) {

      }

      public void onException(Exception ex) {
        ex.printStackTrace();
        feedReceiver.onFailure(ex);
      }
    };

    FilterQuery fq = new FilterQuery();
    fq.track(keywords);

    stream.addListener(listener);
    stream.filter(fq);

  }

  @Override
  public void run() {
    start();
  }
}
