package com.beehyv.twitter;

import twitter4j.Status;

/**
 * Created by kishore on 29/9/17.
 *
 * Classes that receive the twitter feed should implement this interface
 */
public interface FeedReceiver {

  void onTweetReceived(Status status);

  void onFailure(Exception e);

}
