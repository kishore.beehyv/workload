package com.beehyv.elastic;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import com.beehyv.twitter.FeedReceiver;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Random;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import twitter4j.Status;
import twitter4j.User;

/**
 * Created by kishore on 28/9/17.
 */
public class ElasticFeedReceiver implements FeedReceiver {

  private Client client;
  private static final String[] INDICES = {"twitter", "facebook", "instagram", "linkedin", "skype",
      "beehyv", "nokia", "apple", "samsung", "youtube"};
  private static final String TYPE_NAME = "tweet";
  private static final String CLUSTER_NAME = "opas-es-cluster";
  private Random random = new Random();

  public ElasticFeedReceiver() throws UnknownHostException {
    Settings settings = Settings.builder()
        .put("cluster.name", CLUSTER_NAME).build();
    client = new PreBuiltTransportClient(settings)
        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("ip-172-31-31-136.us-west-2.compute.internal"), 9300))
        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("ip-172-31-25-166.us-west-2.compute.internal"), 9300))
        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("ip-172-31-19-137.us-west-2.compute.internal"), 9300));
  }

  @Override
  public void onTweetReceived(Status status) {
    User user = status.getUser();
    String username = user.getScreenName();
    System.out.println(username);
    String profileLocation = user.getLocation();
    System.out.println(profileLocation);
    long tweetId = status.getId();
    System.out.println(tweetId);
    String content = status.getText();
    System.out.println(content +"\n");
    try {
      XContentBuilder builder = jsonBuilder()
          .startObject()
          .field("user", username)
          .field("location", profileLocation)
          .field("tweetId", tweetId)
          .field("content", content)
          .endObject();

      IndexResponse response = client.prepareIndex(INDICES[random.nextInt(INDICES.length)], TYPE_NAME)
          .setSource(builder.string())
          .get();
      System.out.println(response);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void onFailure(Exception e) {
    System.out.println("Exception occurred in Streaming API");
    // client.close();
  }
}
